/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Cukrus
 */
public class Player {
    
    private String name;
    private int dice;
    private Gender gender;
    
    public Player(String name, Gender gender) {
        this.name = name == null ? name : name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
        this.dice = random(1, 6);
        this.gender = gender;
    }
    
    public String getName() {
        return name;
    }

    public int getDice() {
        return dice;
    }

    public Gender getGender() {
        return gender;
    }
    
}
