/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.text.SimpleDateFormat;
import java.util.Date;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Cukrus
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        out("Sveiki atvyke i kauliuku zaidima dviem");
        out("Iveskite pirmo zaidejo varda");
        out("O po to iveskite pirmo zaidejo lyti: 0 - moteris; 1- vyras; 2 - kita");
        Player p1 = new Player(inWord(), Gender.getById(inInt()));
        out("Iveskite antro zaidejo varda");
        out("O po to iveskite antro zaidejo lyti: 0 - moteris; 1- vyras; 2 - kita");
        Player p2 = new Player(inWord(), Gender.getById(inInt()));
        out(p1.getName() + " isrideno " + p1.getDice());
        out(p2.getName() + " isrideno " + p2.getDice());
        if (p1.getDice() > p2.getDice()) {
            out("laimejo " + p1.getName());
        } else if (p1.getDice() == p2.getDice()) {
            out("lygiosios ");
        } else {
            out("laimejo " + p2.getName());
        }
        out(Gender.FEMALE.getEnLabel());
    }
    
    private static void pradziosKodas() {
        out("Iveskite 3 raidziu zodi");
        String abc = null;
        String zxc = null;
        if (abc != null && abc.equals(zxc)) {
            out("tikrai abc");
        } else {
            out("" + 7);
        }
        String result = (abc != null && abc.equals(zxc)) ? "tikrai abc" : "ne abc";
        out(result);
        out(new Date());
        SimpleDateFormat sdf = new SimpleDateFormat("'Data: 'yyyy-MM-dd 'Laikas: 'HH:mm:ss:SSS");
        out(sdf.format(new Date()));
        out(random(1, 10));
    }
    
}
