/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

/**
 *
 * @author Cukrus
 */
public interface Named extends Idable {
    
    static Object newObj(){
        return new Object();
    }
    
    String getName() throws BadDataInputException;
    
}
