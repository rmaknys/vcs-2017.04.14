/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.util.ArrayList;
import java.util.List;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Cukrus
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Named g1 = Gender.FEMALE;
        Named g2 = null;
        try {
            g2 = new Person("sdfsdf", "sdfsdf");
        } catch (Exception e) {
        }
        List<Named> namedList = new ArrayList();
        namedList.add(g1);
        namedList.add(g2);
        Named.newObj();
        for (Named named : namedList) {
            try {
                out(named.getName());
                if (named instanceof Idable) {
                    Idable idable = (Idable) named;
                    out(idable.getId());
                }
            } catch (Exception e) {
            }
        }
        out("belenkas:");
        AbstraktusDaiktas ad = new Belenkas();
        ad.bendrasFunkcionalumas();
        out("belenkas2:");
        AbstraktusDaiktas ad2 = new Belenkas2();
        ad2.bendrasFunkcionalumas();
    }

}
