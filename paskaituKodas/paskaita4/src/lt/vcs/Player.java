/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Cukrus
 */
public class Player extends Person {
    
    private int dice;
    
    public Player(String name) {
        super(name);
        this.dice = random(1, 6);
    }
    
    public Player(String name, Gender gender){
        this(name);
        this.gender = gender;
    }
    
    public Player(String name, String surName, Gender gender, int age){
        this(name, gender);
        setSurName(surName);
        setAge(age);
    }
    
    @Override
    public String toString() {
        return super.toString().replaceFirst("\\)", " dice=" + dice + ")");
    }

    public int getDice() {
        return dice;
    }
    
}
