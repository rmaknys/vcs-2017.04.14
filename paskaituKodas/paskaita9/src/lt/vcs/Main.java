/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import static lt.vcs.VcsUtils.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Cukrus
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String url = "jdbc:mysql://localhost:3306/";
        // String driver = "com.mysql.jdbc.Driver";
        String dbName = "vcs_17_04";
        String userName = "root";
        String password = "";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url + dbName, userName, password);
            out("Valio!!! prisijungem prie DB, schema: " + conn.getSchema());
            Statement s = conn.createStatement();
            s.executeUpdate("insert into person values("
                    + (getLastId(s, "person") + 1)
                    + ", 'As', 'Mano', 2, 77, 'as@mano.lt');");
            ResultSet rs = s.executeQuery("select * from person;");
            while (rs.next()) {
                String name = rs.getString(2);
                String email = rs.getString("email");
                int id = rs.getInt(1);
                int gender = rs.getInt("gender");
                out("name: " + name + "\t\temail: " + email + "\t\tid: " + id
                    + "\t\tgender: " + gender);
                out("name: " + rs.getMetaData().getColumnName(2)
                        + " type: " + rs.getMetaData().getColumnType(2));
            }
        } catch (Exception e) {
            out("Nepavyko prisijungti prie DB: " + e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
        }
    }
    
    private static Integer getLastId(Statement s, String table) {
        Integer result = null;
        try {
            ResultSet rs = s.executeQuery("select id from " + table 
                    + " order by id desc limit 1;");
            if (rs.next()) {
                result = rs.getInt(1);
            }
        } catch (Exception e) {
            out("Klaida traukiant paskutini id is " + table + ": " + e.getMessage());
        }
        return result;
    }
    
}
