package lt.vcs;

import static lt.vcs.VcsUtils.*;

/**
 * Zaidimo klase, reprezentuojanti viena zaidimo partija
 */
public class Game {

    /** pirmas zaidejas */
    private final Player p1;
    /** antras zaidejas */
    private final Player p2;

    private Player activePlayer;

    /**
     * zaidimo konstruktorius
     * @param p1 pirmas zaidejas
     * @param p2 antras zaidejas
     */
    public Game(Player p1, Player p2) {
        this.p1 = p1;
        this.p2 = p2;
        activePlayer = p1;
    }

    /**
     * startuoja zaidima/partija
     * @return Player objekta - zaideja, kuris laimejo partija
     */
    public Player start() {
        //cia tik pvz kaip galima nustatyti laimetoja
        Player laimetojas = GameUtils.kasLaimejo(p1, p2);
        return laimetojas;
    }
    
    private int getTotalPot(int pot, Combination combo) {
        return pot + combo.getBonus();
    }
    
    private Player nextActive() {
        return Main.getNextActivePlayer(this);
    }

    public Player getP1() {
        return p1;
    }

    public Player getP2() {
        return p2;
    }

    public Player getActivePlayer() {
        return activePlayer;
    }

    public void setActivePlayer(Player activePlayer) {
        this.activePlayer = activePlayer;
    }
}
