/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Cukrus
 */
public class Main {
    
    public static final String SELECT_LAST_ID_SQL = "select MAX(id) from %s;";

    static String url = "jdbc:mysql://localhost:3306/";
    // String driver = "com.mysql.jdbc.Driver";
    static String dbName = "vcs_17_04";
    static String userName = "root";
    static String password = "";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    Connection conn = null;
        try {
            conn = DriverManager.getConnection(url + dbName, userName, password);
            Statement s = conn.createStatement(
                    ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = s.executeQuery("select * from car;");
            out("------------ car lenteles turinys (pries insert) -------------");
            outCarResultSet(rs);
//            for (int i = 1; rs.next(); i++) {
//                rs.updateString("make", rs.getString("make")
//                        + " (kazka kompensuoja) " + i);
//                rs.updateRow();
//            }

            rs.moveToInsertRow();
            rs.updateInt(1, getLastId("car") + 1);
            rs.updateString("make", "ZAPUKAS");
            rs.updateString("name", "sens bet gers ir raudons");
            rs.insertRow();
            out("------------ car lenteles turinys (po insert) -------------");
            outCarResultSet(rs);
            rs.afterLast();
            rs.previous();
            rs.deleteRow();
            out("------------ car lenteles turinys (po delete) -------------");
            outCarResultSet(rs);
        } catch (Exception e) {
            out("Ivyko klaida: " + e.getMessage());
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    out(e.getMessage());
                    throw new RuntimeException(e);
                }
            }
        }
    }

    private static void outCarResultSet(ResultSet rs) throws Exception {
        rs.beforeFirst();
        while (rs.next()) {
            out("id='" + rs.getInt(1) + "' make='" + rs.getString(2)
                    + "' name='" + rs.getString(3) + "'");
        }
        rs.beforeFirst();
    }

    private static Integer getLastId(String table) {
        Integer result = null;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url + dbName, userName, password);
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery(String.format(SELECT_LAST_ID_SQL, table));
            if (rs.next()) {
                result = rs.getInt(1);
            }
        } catch (Exception e) {
            out("Klaida traukiant paskutini id is " + table + ": " + e.getMessage());
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    out(e.getMessage());
                    throw new RuntimeException(e);
                }
            }
        }
        return result;
    }

}
