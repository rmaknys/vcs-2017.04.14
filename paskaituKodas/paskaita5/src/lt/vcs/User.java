/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

/**
 *
 * @author Cukrus
 * @param <T> Person tipo parametras
 */
public class User<T extends Person> {
    
    private T person;
    
    public User(T person) {
        this.person = person;
    }

    public T getPerson() {
        return person;
    }
    
}
